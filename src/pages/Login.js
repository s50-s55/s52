import {Form, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';



export default function Login() {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false)

function loginUser(e) {

	e.preventDefault();


	if (email == email && password == password) {

		setEmail("");
		setPassword("");

		alert("You are now Logged in")

	} else {

		setEmail("");
		setPassword("");

		alert("Wrong Password & Email")
	}
}


useEffect (() => {

	if ((email !== "" && password !== "" ) && (password.toLowerCase() !== password) && (password.toUpperCase() !== password)) {

		setIsActive(true)
	} else {

		setIsActive(false)
	}

})


	return (

		<Form onSubmit={(e) => loginUser(e)}>
			<h2>Login</h2>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter your email"

					value = {email}
					onChange = {e => setEmail(e.target.value)}

					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Enter your email"

					value = {password}
					onChange = {e => setPassword(e.target.value)}

					required
				/>
			</Form.Group>
              
              { isActive ?

			<Button variant="success my-3" type="submit" id="submitBtn">Login</Button>
			:
			<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Login</Button>

			  }
		</Form>

		)
}